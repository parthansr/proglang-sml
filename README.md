__Author__ : Parthan <parthan@technofreak.in>

#About

This repository contains problem solutions and practice problems from the course Programming Languages in Coursea, specifically the initial part covering the functional programming aspects using ML programming language (SML). There will be subsequent repositories for the same course making use of Racket and Ruby.

# Course

## Week 1
+ ML Variable Bindings and Expressions
+ Rules for Expressions
+ The REPL and Errors
+ Shadowing
+ Functions Informally and Formally
+ Pairs and Other Tuples
+ Introducing Lists
+ List Functions
+ Let Expressions
+ Nested Functions
+ Let and Efficiency
+ Options
+ Boolean and Comparison Operations

### Programming Assignment
1. Calendar functions - is_older, number_in_month, number_in_months, dates_in_month, dates_in_months, get_nth, date_to_string, number_before_reaching_sum, what_month, month_range, oldest
2. *Challenger Problem* number_in_months_challenge and dates_in_months_challenge that can handle duplicate month values
3. *Challenger Problem* reasonable_date that can validate if a given date is real

## Week 2
+ Building Compound Types
+ Records
+ Tuples as Syntactic Sugar
+ Datatype Bindings
+ Case Expressions
+ Useful Datatypes
+ Pattern Matching So Far
+ Another Expression Example
+ Type Synonyms
+ Lists and Options are Datatypes
+ Polymorphic Datatypes
+ Each of Pattern Matching / Truth about Functions
+ A Little Type Inference
+ Polymorphic and Equality Types
+ Nested Patterns
+ More Nested Patterns
+ Nested Patterns Precisely
+ Function Patterns
+ Exceptions
+ Tail Recursion
+ Accumulators for Tail Recursion
+ Perspective on Tail Recursion