(* Programming Languages Assignment 1 *)

(* function is_older takes two dates and evaluates to true or false *)
(* fn : (int * int * int) * (int * int * int) -> bool *)
fun is_older (first : int * int * int, second : int * int * int) =
    (#1 first < #1 second) 
    orelse (#1 first = #1 second andalso #2 first < #2 second)
    orelse (#1 first = #1 second andalso #2 first = #2 second andalso
	    #3 first < #3 second)


(* function number in month takes a list of dayes and a month, and returns *)
(* how many dates in the list are in the given month *)
(* fn : (int * int * int) list * int -> int *)
fun number_in_month (dates : (int * int * int) list, m : int) =
    if null dates
    then 0
    else 
	if #2 (hd dates) = m then
	    1 + number_in_month(tl dates, m)
	else
	    number_in_month(tl dates, m)

	
(* function number_in_months takes a list of dates and a list of months *)
(* and returns the number of dates in the list that in any of the months *)
(* fn : (int * int * int) list * int list -> int *)
fun number_in_months (dates : (int * int * int) list, months : int list) =
    if null dates orelse null months
    then 0
    else
	let 
	    val count = number_in_month(dates, hd months)
	in
	    if count > 0
	    then count + number_in_months(dates, tl months)
	    else number_in_months(dates, tl months)
	end


(* function dates_in_month takes a list of dates and a month, and returns *)
(* a list holding the dates that are in the month *)
(* fn : (int * int * int) list * int -> (int * int * int) list *)
fun dates_in_month (dates : (int * int * int) list, m : int) =
    if null dates
    then []
    else
	if #2 (hd dates) = m then
	    hd dates :: dates_in_month(tl dates, m)
	else
	    dates_in_month(tl dates, m)


(* function dates_in_months takes a list of dates and list of months, and returns *)
(* a list of dates that are in any of the months *)
(* fn : (int * int * int) list * int list -> (int * int * int) list *)
fun dates_in_months (dates : (int * int * int) list, months : int list) =
    if null dates orelse null months
    then []
    else
	let
	    val cumdates = dates_in_month(dates, hd months)
	    fun append (xs : (int * int * int) list, ys : (int * int * int) list) =
		if null xs
		then ys
		else (hd xs) :: append(tl xs, ys)
	in
	    if null cumdates
	    then dates_in_months(dates, tl months)
	    else append(cumdates, dates_in_months(dates, tl months))
	end


(* function get_nth takes a list of string and an int, and returns nth element *)
(* fn : string list * int -> string *)
fun get_nth (slist : string list, n : int) =
    if n = 1
    then hd slist
    else get_nth(tl slist, n-1)


(* function date_to_string takes a date and returns a string of the form *)
(* January 20, 2013 *)
(* fn : int * int * int -> string *)
fun date_to_string (date : int * int * int) =
    let
	val months = ["January","February","March","April","May","June","July","August","September","October","November","December"]
	val year = Int.toString(#1 date)
	val month = get_nth(months, #2 date)
	val day = Int.toString(#3 date)
    in
	month ^ " " ^ day ^ ", " ^ year
    end


(* function number_before_reaching_sum takes an int sum and an int list, and returns *)
(* an int n such that first n elements in list add to less than sum, but the first *)
(* n+1 elements in the list add to sum or more *)
(* fn : int, int list -> int *)
fun number_before_reaching_sum (sum : int, xs : int list) =
    let
	fun iter (count : int, sum : int, xs : int list) =
	    if sum - hd xs <= 0 then
		count
	    else
		iter(count+1, sum - hd xs, tl xs)
    in
	iter(0, sum, xs)
    end


(* function what_month takes a day of year, int between 1 and 365, and returns *)
(* an int for the month of the year *)
(* fn : int -> int *)
fun what_month (totdays : int) =
    let
	val monthdays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    in
	number_before_reaching_sum(totdays, monthdays) + 1
    end


(* function month_range takes two days of the year day1 and day2, and returns *)
(* int list [m1, m2,..., mn] where m1 is month of day1 and mn is month of day2 *)
(* fn :  int * int -> int list *)
fun month_range (day1 : int, day2 : int) =
    if day1 > day2 then
	[]
    else
	what_month(day1) :: month_range(day1+1, day2)


(* function oldest takes a list of dates and evaluates to an (int*int*int) option *)
(* evaluates to NONE if list is empty and SOME d if d is the oldest date *)
(* fn : (int * int * int) list -> (int * int * int) option *)
fun oldest (dates : (int * int * int) list) =
   if null dates then
       NONE
   else
       let
	   val old = oldest(tl dates)
       in
	   if isSome old andalso is_older(valOf old, hd dates)
	   then old
	   else SOME (hd dates)
       end


(* Challenge 1 : functions number_in_months_challenge and dates_in_months_challenge *)
(* that can handle duplicates in months *)
fun contains (x : int, xs : int list) =
    if null xs then
	false
    else if hd xs = x then
	true
    else
	contains(x , tl xs)

fun append (xs : int list, ys : int list) =
    if null xs
    then ys
    else (hd xs) :: append(tl xs, ys)

fun remove_duplicates (xs : int list) =
    let
	fun iter (xs : int list, ys : int list) =
	    if null xs then
		ys
	    else if contains(hd xs, ys) then
		iter(tl xs, ys)
	    else
		iter(tl xs, append(ys, [hd xs]))
    in
	iter(xs, [])
end

(* fn : (int * int * int) list * int list -> int *)
fun number_in_months_challenge(dates: (int * int * int) list, months : int list) =
    if null dates orelse null months then
	0
    else
	number_in_months_challenge(dates, remove_duplicates(months))

fun dates_in_months_challenge(dates: (int * int * int) list, months: int list) =
    if null dates orelse null months then
	0
    else
	dates_in_months_challenge(dates, remove_duplicates(months))

(* Challenge 2 : function reasonable_date takes a date and determines if it is real *)
(* fn : int * int * int -> bool *)
fun reasonable_date (date : int * int * int) =
    let
	fun leap_year (yr : int) =
	    yr mod 400 = 0 orelse (yr mod 100 <> 0 andalso yr mod 4 = 0)
	fun index (xs : int list, n : int) =
	    if n = 1 then
		hd xs
	    else
		index(tl xs, n-1)
	val monthdays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
	val yr = #1 date
	val mon = #2 date
	val day = #3 date
    in
	if yr < 1 orelse mon < 1 orelse mon > 12 orelse day < 1 orelse day > 31 then
	    false
	else if leap_year yr andalso mon = 2 then
	    day <= 29
	else
	    day <= index(monthdays, mon)
    end 
