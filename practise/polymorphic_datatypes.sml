
fun sum_list xs =
    case xs of
	[] => 0
     | x::xs' => x + sum_list xs'

fun append (xs, ys) =
    case xs of
	[] => ys
     | x::xs' => x :: append(xs', ys) 

datatype 'a option = NONE | SOME of 'a

datatype 'a mylist = Empty | Cons of 'a * 'a mylist

(* a fancier example for binary trees where internal nodes have data of one data type and leaves have data of another different type *)
datatype ('a, 'b) tree = Node of 'a * ('a, 'b) tree * ('a, 'b) tree
                         | Leaf of 'b

(* type is (int, int) tree -> int *)
fun sum_tree tr =
	case tr of
	    Leaf i => i
	 | Node (i, lft, rgt) => i + sum_tree lft + sum_tree rgt

(* type is ('a, int) tree -> int *)
fun sum_leaves tr =
    case tr of
	Leaf i => i
     | Node (i, lft, rgt) => sum_leaves lft + sum_leaves rgt 

fun num_leaves tr =
    case tr of
	Leaf i  => 1
     | Node (i, lft, rgt) => num_leaves lft + num_leaves rgt
