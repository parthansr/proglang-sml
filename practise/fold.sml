
fun fold(f, acc, xs) =
    case xs of
	[] => acc
     | x::xs' => fold (f, f(acc, x), xs')

fun f1 xs = fold ((fn (x, y) => x+y), 0, xs) 

(* are all list elements non-negative *)
fun f2 xs = fold ((fn (x,y ) => x andalso y >= 0), true, xs)

(* counting number of items that are between lo and hi *) 
fun f3 (xs, lo, hi) =
    fold ((fn (x, y) =>
	      x + (if y >= l0 andalso y <= hi then 1 else 0)),
	  0, xs)

(* are all elements of the list less than size of s *)
fun f4 (xs, s) =
    let
	val i = String.size s
    in
	fold((fn (x, y) => x andalso String.size y < i), true, xs)
    end

(* checks all for function g *)
fun f5 (g, xs) =
    fold((fn (x, y) => x andalso g y), true, xs)

fun f4v2 (xs, s) -
    let
	val i  = String.size s
    in
	f5(fn y => String.size y < i, xs)
    end
