exception ListLengthMismatch 

fun old_zip (l1, l2, l3) =
    if null l1 andalso null l2 andalso null l3
    then []
    else
	if null l1 orelse null l2 orelse null l3
	then
	    raise ListLengthMismatch
	else
	    (hd l1, hd l2, hd l3) :: old_zip(tl l1, tl l2, tl l3)

fun zip list_triple =
    case list_triple of
	([], [], []) => []
     | (hd1::tl1, hd2::tl2, hd3::tl3) => (hd1, hd2, hd3) :: zip(tl1, tl2, tl3)
     | _ => raise ListLengthMismatch 

fun unzip lst =
    case lst of
	[] => ([], [], [])
     | (a,b,c)::tl => let val (l1, l2, l3) = unzip tl
		      in
			  (a::l1, b::l2, c::l3)
		      end01, b::l2, c::l3)
		      end

fun nondecreasing xs = (* int list -> bool *)
    case xs of
	[] => true
     | _::[] => true
     | head::(neck::rest) => head <= neck 
			     andalso nondecreasing (neck::rest)

datatype sgn = P | N | Z

fun multsign (x1, x2) = (* int * int -> sgn *)
    let fun sign x = if x=0 then z else x>0 then P else N
    in
	case (sign x1, sign x2) of
	    (Z, _) => Z
	 | (_,Z) => Z
	 | (P,P) => P
	 | _ => N
    end 

fun len xs =
    case xs of
	[] => 0
     | _::xs' => 1 + len xs'

