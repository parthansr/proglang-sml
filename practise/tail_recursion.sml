(* inefficient although simle *)
fun fact n = if n=0 then 1 else n*fact(n-1)

val x1 = fact 9

fun fact2 n =
    let fun aux(n, acc) =
	    if n=0
	    then acc
	    else aux(n-1, acc*n)
    in
	aux(n, 1)
    end

val x2 = fact 9

fun sum xs =
    let fun aux(xs, acc) =
	    case xs of
		[] => acc
	     | x::xs' => aux(xs', x+acc)
    in
	aux(xs, 0)
    end

(* inefficient *)
fun rev0 xs =
    case xs of
	[] => []
     | x::xs' => (rev0 xs') @ [x] 

fun rev xs =
    let fun aux(xs, acc) =
	    case xs of
		[] => acc
	     | x::xs' => aux(xs', x::acc)
    in
	aux(xs, [])
    end
