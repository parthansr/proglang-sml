(* bad style *)
fun sum_tripe_1 triple =
    case triple of
	(x, y, z) => x + y + z

fun full_name_1 r =
    case r of
	{first=x, middle=y, last=z} =>
	x ^ " " ^ y ^ " " ^ z

(* something better *)
fun sum_triple_2 triple =
    let val (x, y, z) = triple
    in
	x + y + z
    end

fun full_name_2 r =
    let val {first=x, middle=y, last=z} = r
    in
	x ^ " " ^ y ^ " " ^ z
    end

(* best and ideal *)
fun sum_triple (x, y, z) =
    x + y + z

fun full_name {first=x, middle=y, last=z} =
    x ^ " " ^ y ^ " " z

fun rotate_left (x, y, z) = (y, z, x)
fun rotate_right t = rotate_left(rotate_left t)

(* fn : int * 'a * int *)
fun partial_sum (x, y, z) =
    x + z

(* fn : {first:string, middle='a, last=string} *)
fun partial_name {first=x, middle=y, last=z} =
    x ^ " " ^ z

fun append (xs, ys) =
    case xs of
	[] => ys
     | x::xs' => x :: append(xs', ys)
