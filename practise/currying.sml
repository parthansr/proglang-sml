
fun sorted_tupled (x, y, z) = z >= y andalso y >= x

val t1 = sorted_tupled (7, 9, 11)

val sorted = fn x => fun y => fun z => z >= y andalso y >= x

val t2 = sorted 7 9 11

fun sorted_nicer x y z = z >= y andalso y >= x

val t3 = sorted_nicer 7 9 11
val t4 = ((sorted_nicer 7) 9) 11

fun fold f acc xs =
    case xs of
	[] => acc
     | x::xs' => fold f (f(acc, x)) xs'

fun sum xs = fold (fn (x,y) => x+y) 0  xs

