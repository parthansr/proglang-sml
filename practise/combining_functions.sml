
fun compose (f, g) = fn x => f(g x)

val sqrt_of_abs = Match.sqrt o Real.fromInt o abs

infix !>

fun x !> f = f x

fun sqrt_of_abs2 i = i !> abs !> Real.fromInt !> Match.sqrt

fun backup (f, g) = fn x => 
		      case x of 
			  None => g x
		       | SOME y => y

fun backup2 (f, g) = fn x => f x handle _ => g x
