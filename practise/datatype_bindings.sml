(* Records *)
val customer = { fname="Robert", lname="Langdon", isNew=true }
val monthrec = { 1="January", 2="February", 3="March" }
val monthtuple = ("January","February","March")

val cust_name = #fname customer ^ " " ^ #lname customer
(* Tuple is Syntactic Sugar for Record *)
val first_month_rec = #1 monthrec
val first_month_tup = #1 monthtuple

datatype identifier = ID of int | TranID of string | UUID | Paired of string * int
val a = TranID "HA8SA3"
val b = ID 200176
val c = UUID
val d = Paired ("20141011", 376)

(* identifier -> strimg *)
fun f (x : identifier) =
    case x of
	UUID => "NONE"
     | ID i => Int.toString(i)
     | TranID s => s 
     | Paired (ts, ti) => ts ^ "_" ^ Int.toString(ti)  

datatype suit = Club | Diamond | Heart | Spade
datatype rank = Jack | Queen | King | Ace | Num of int

datatype id = StudentNum of int
       | Name of string * (string option) * string

datatype exp = Constant of int
       | Negate of exp
       | Add of exp * exp
       | Multiply of exp * exp

val exexp = Add (Constant (10+9), Negate (Constant 4))

fun eval e =
    case e of 
	Constant i => i
     | Negate e2 => ~ (eval e2)
     | Add (e1, e2) => (eval e1) + (eval e2)
     | Multiply (e1, e2) =>  (eval e1) * (eval e2)

fun number_of_adds e =
    case e of 
	Constant i => 0
     | Negate e2 => number_of_adds e2
     | Add (e1, e2) => 1 + number_of_adds e1 + number_of_adds e2
     | Multiply (e1, e2) => number_of_adds e1 + number_of_adds e2

val exans = eval exexp
val exadds = number_of_adds exexp
